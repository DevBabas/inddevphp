<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
require_once __DIR__ . "/../src/Services/ClientService.php";


final class ClientServiceTest extends TestCase
{
    private $clientService;

    protected function setUp(): void
    {
        $this->clientService = new ClientService();
    }

    public function testCanAddClient(): void
    {
        $client = new Client('Claudia', 'WEMBE', '07 10 34 56 78');
        $newclient = $this->clientService->addclient($client);
        $this->assertNotNull($newclient->getId());
        $this->assertSame($client->getFirstname(), $newclient->getFirstname());
        $this->assertSame($client->getLastname(), $newclient->getLastname());
        $this->assertSame($client->getPhone(), $newclient->getPhone());
    }

    public function testAddClientInvalidFirstname()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Invalid firstname.");
        $client = new Client(null, 'WEMBE', '06 19 34 56 78');
        $this->clientService->addClient($client);
    }

    public function testAddClientInvalidLastname()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Invalid lastname.");
        $client = new Client('Claudia', null, '07 12 34 96 78');
        $this->clientService->addclient($client);
    }

    public function testAddClientWithEmptyFirstname()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Firstname cannot be empty.");

        $client = new Client(' ', 'WEMBE', '07 12 34 56 58');
        $this->clientService->addClient($client);
    }

    public function testAddClientWithEmptyLastname()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Lastname cannot be empty.");

        $client = new client('Claudia', ' ', '06 12 34 16 78');
        $this->clientService->addClient($client);
    }

    public function testAddClientWithEmptyPhone()
{
    $this->expectException(InvalidArgumentException::class);
    $this->expectExceptionMessage("Invalid PHONE.");

    $client = new Client('Claudia', 'WEMBE', ''); 
    $this->clientService->addClient($client);
}

    public function testAddClientInvalidPhone()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Invalid PHONE.");
        $client = new Client('Claudia', 'WEMBE', null);
        $this->clientService->addClient($client);
    }

    public function testAddClientWithInvalidPhoneFormat()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Invalid PHONE.");

        $client = new Client('Claudia', 'WEMBE', '00 00 00 00');
        $this->clientService->addClient($client);
    }

    public function testFindByIdWithValidId()
    {
        $client = new Client('Claudia', 'WEMBE', '06 02 34 56 78');
        $newclient = $this->clientService->addClient($client);
        $foundclient = $this->clientService->findById($newclient->getId());
        $this->assertNotNull($foundclient);
        $this->assertSame($client->getFirstname(), $foundclient->getFirstname());
        $this->assertSame($client->getLastname(), $foundclient->getLastname());
        $this->assertSame($client->getPhone(), $foundclient->getPhone());
    }

    public function testFindByIdWithNullId()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("ID is required.");

        $this->clientService->findById(null);
    }

    public function testFindByIdWithEmptyStringId()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("ID is cannot be empty.");

        $this->clientService->findById(' ');
    }


    public function testFindByIdWithInvalidId()
    {
        $foundclient = $this->clientService->findById(-1);
        $this->assertNull($foundclient);
    }

    public function testFindByIdWithNonExistentId()
    {
        $foundclient = $this->clientService->findById(99*99*99*99*99);
        $this->assertNull($foundclient);
    }

    public function testUpdateClientSuccessfully()
    {
        $client = new client('Claudia', 'WEMBE', '06 12 34 56 70');
        $newclient = $this->clientService->addclient($client);
        $foundclient = $this->clientService->findById($newclient->getId());
        $foundclient->setLastname("Doe");
        $foundclient->setFirstname("Jane");
        $foundclient->setPhone('06 10 34 56 78');
        $result = $this->clientService->UpdateClient($foundclient);
        $foundNewclient = $this->clientService->findById($newclient->getId());
        $this->assertTrue($result);
        $this->assertSame("Jane", $foundNewclient->getFirstname());
        $this->assertSame("Doe", $foundNewclient->getLastname());
        $this->assertSame('06 10 34 56 78', $foundNewclient->getPhone());
    }
    public function testUpdateClientWithoutId()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("ID is required for update.");
        $client = new client('Claudia', 'WEMBE', '06 12 34 56 71');
        $this->clientService->UpdateClient($client);
    }

    public function testUpdateClientNotFound()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Not found.");
        $client = new client('Claudia', 'WEMBE', '06 12 36 56 78');
        $client->setId(9*9*9*9*9*9*9*9*9*9*9*9*9*9*9*9*9*9*9*9*9*9);
        $this->clientService->UpdateClient($client);
    }

    public function testUpdateClientInvalidFirstname()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Firstname is required for update.");
        $client = new client('Claudia', 'WEMBE', '07 12 14 56 78');
        $newclient = $this->clientService->addclient($client);
        $foundclient = $this->clientService->findById($newclient->getId());
        $foundclient->setLastname("Doe");
        $foundclient->setFirstname(null);
        $foundclient->setPhone('06 12 34 50 78');
        $this->clientService->UpdateClient($foundclient);
    }

    public function testUpdateClientInvalidLastname()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Lastname is required for update.");
        $client = new client('Claudia', 'WEMBE', '06 62 34 56 78');
        $newclient = $this->clientService->addclient($client);
        $foundclient = $this->clientService->findById($newclient->getId());
        $foundclient->setLastname(null);
        $foundclient->setFirstname("Jane");
        $foundclient->setPhone('');
        $this->clientService->UpdateClient($foundclient);
    }

    public function testUpdateClientWithEmptyFirstname()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Firstname cannot be empty.");
        $client = new client('Claudia', 'WEMBE', '06 12 34 06 78');
        $newclient = $this->clientService->addclient($client);
        $foundclient = $this->clientService->findById($newclient->getId());
        $foundclient->setLastname("Doe");
        $foundclient->setFirstname(" ");
        $foundclient->setPhone('06 12 34 58 78');
        $this->clientService->UpdateClient($foundclient);
    }

    public function testUpdateClientWithEmptyLastname()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Lastname cannot be empty.");
        $client = new client('Claudia', 'WEMBE', '07 12 34 56 78');
        $newclient = $this->clientService->addclient($client);
        $foundclient = $this->clientService->findById($newclient->getId());
        $foundclient->setLastname(" ");
        $foundclient->setFirstname("John");
        $foundclient->setPhone('07 12 34 56 78');
        $this->clientService->UpdateClient($foundclient);
    }

    public function testUpdateClientWithEmptyPhone()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("PHONE cannot be empty.");
        $client = new client('Claudia', 'WEMBE', '06 12 34 56 78');
        $newclient = $this->clientService->addclient($client);
        $foundclient = $this->clientService->findById($newclient->getId());
        $foundclient->setLastname("Doe");
        $foundclient->setFirstname("John");
        $foundclient->setPhone(" ");
        $this->clientService->UpdateClient($foundclient);
    }

    public function testUpdateClientInvalidPhone()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("PHONE is required for update.");
        $client = new client('Claudia', 'WEMBE', '06 12 34 56 78');
        $newclient = $this->clientService->addclient($client);
        $foundclient = $this->clientService->findById($newclient->getId());
        $foundclient->setLastname("Joe");
        $foundclient->setFirstname("John");
        $foundclient->setPhone(null);
        $this->clientService->UpdateClient($foundclient);
    }

    public function testUpdateClientWithInvalidPhoneFormat()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Invalid PHONE.");
    
        $client = new Client('Claudia', 'WEMBE', '06 12 34 56 88');
        $newClient = $this->clientService->addClient($client);
        
        $foundClient = $this->clientService->findById($newClient->getId());
        $foundClient->setLastname("Joe");
        $foundClient->setFirstname("John");
        $foundClient->setPhone("invalid");
    
        $this->clientService->updateClient($foundClient);
    }
    

   
    public function testDeleteclientWithValidId()
    {
        $client = new client('Claudia', 'WEMBE', '06 12 14 56 78');
        $newclient = $this->clientService->addclient($client);
        $result = $this->clientService->deleteclient($newclient->getId());
        $this->assertTrue($result);
    }

    public function testDeleteclientWithNullId()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("ID is required for deletion.");
        $this->clientService->deleteclient(null);
    }

    public function testDeleteclientNotFound()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage("Not found.");
        $id = 9*9*6*69*9*99*9*9*9*9*9*9*9*9*9*9;
        $this->clientService->deleteclient($id);
    }

    public function testFindAll()
    {
        $clients = $this->clientService->findAll();
        $this->assertIsArray($clients);
    }
}
