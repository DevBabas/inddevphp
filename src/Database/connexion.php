<?php
try{
    $pdo = new PDO('sqlite:'.dirname(__FILE__).'/database.sqlite');
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    // ERRMODE_WARNING | ERRMODE_EXCEPTION | ERRMODE_SILENT
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(Exception $e) {
    echo "Impossible d'accéder à la base de données SQLite : ".$e->getMessage();
    die();
}


$pdo->query("CREATE TABLE IF NOT EXISTS client (
    id            INTEGER         PRIMARY KEY AUTOINCREMENT,
    firstname         VARCHAR( 50 ),
    lastname       VARCHAR( 50 ),
    phone      INTEGER
);");


