<?php
declare(strict_types=1);

require_once __DIR__ . "/../Repository/ClientRepository.php";

class ClientService
{
    private $clientRepository;


    public function __construct()
    {
        $this->clientRepository = new ClientRepository();
    }

    public function addClient(Client $client): ?Client
    {

        if($client->getFirstname() == null) {
            throw new InvalidArgumentException("Invalid firstname.");
        }

        if($client->getLastname()  == null) {
            throw new InvalidArgumentException("Invalid lastname.");
        }

        if($client->getPhone()  == null){
            throw new InvalidArgumentException("Invalid PHONE.");
        }

        if($client->getFirstname() == ' ') {
            throw new InvalidArgumentException("Firstname cannot be empty.");
        }

        if($client->getLastname()  == ' ') {
            throw new InvalidArgumentException("Lastname cannot be empty.");
        }

        if($client->getPhone()  == ' '){
            throw new InvalidArgumentException("PHONE cannot be empty.");
        }
        if (!$this->isValidPhone($client->getPhone())) {
            throw new InvalidArgumentException("Invalid PHONE.");
        }

        
        $idclient = $this->clientRepository->add($client);
        $newclient = $this->findById($idclient);
        return $newclient;
    }
    public function findAll(): array
    {
        $success = $this->clientRepository->findAll();
        return $success;
    }

    public function findById($id): ?client
    {
        if ($id == null) {
            throw new InvalidArgumentException("ID is required.");
        }

        if ($id == ' ') {
            throw new InvalidArgumentException("ID is cannot be empty.");
        }
        $client = $this->clientRepository->findById($id);
        return $client;
    }

    public function updateclient(client $client): bool
    {
        if ($client->getId() == null) {
            throw new InvalidArgumentException("ID is required for update.");
        }

        if($client->getFirstname() == null) {
            throw new InvalidArgumentException("Firstname is required for update.");
        }

        if($client->getLastname()  == null) {
            throw new InvalidArgumentException("Lastname is required for update.");
        }

        if($client->getPhone()  == null){
            throw new InvalidArgumentException("PHONE is required for update.");
        }
        if ($client->getId() == ' ') {
            throw new InvalidArgumentException("ID cannot be empty.");
        }

        if($client->getFirstname() == ' ') {
            throw new InvalidArgumentException("Firstname cannot be empty.");
        }

        if($client->getLastname()  == ' ') {
            throw new InvalidArgumentException("Lastname cannot be empty.");
        }

        if($client->getPhone()  == ' '){
            throw new InvalidArgumentException("PHONE cannot be empty.");
        }

        if (!$this->isValidPhone($client->getPhone())) {
            throw new InvalidArgumentException("Invalid PHONE.");
        }

       

        if ($this->findById($client->getId()) == null) {
            throw new InvalidArgumentException("Not found.");
        }
        $success = $this->clientRepository->update($client);
        return $success;
    }

    function isValidPhone($phone) {
        // Remove non-numeric characters
        $phone = preg_replace('/[^0-9]/', '', $phone);
    
        // Check the length of the number
        if (strlen($phone) != 10) {
            return false;
        }
    
        // Check the prefix for French numbers (0X)
        if (substr($phone, 0, 1) !== '0') {
            return false;
        }
    
        // Check the second digit of the prefix
        $secondDigit = substr($phone, 1, 1);
        if ($secondDigit !== '6' && $secondDigit !== '7') {
            return false;
        }
    
        // The number seems to be in the correct format
        return true;
    }
    

    public function deleteclient($id): bool
    {
        if ($id == null) {
            throw new InvalidArgumentException("ID is required for deletion.");
        }

        if ($this->findById($id) == null) {
            throw new InvalidArgumentException("Not found.");
        }

        $success = $this->clientRepository->delete($id);
        return $success;
    }
}
