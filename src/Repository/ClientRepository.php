<?php
declare(strict_types=1);

require_once __DIR__ . "/../Models/Client.php";
class ClientRepository
{
    private $pdo;

    public function __construct()
    {
        $pathToDb = __DIR__ . '/../data.sqlite';
        $this->pdo = new PDO("sqlite:$pathToDb");
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->pdo->query("CREATE TABLE IF NOT EXISTS client (
            id            INTEGER         PRIMARY KEY AUTOINCREMENT,
            firstname         VARCHAR( 50 ),
            lastname       VARCHAR( 50 ),
            phone  INTEGER
        );");
    }

    public function add(Client $client): ?int
    {
        $stmt = $this->pdo->prepare('INSERT INTO client (firstname, lastname, phone) VALUES (?, ?, ?)');
        $stmt->execute([$client->getFirstname(), $client->getLastname(), $client->getphone()]);
        $id = $this->pdo->lastInsertId();
        $client->setId((int)$id);
        return $client->getId();
    }


    public function findById($id): ?Client
    {
        $stmt = $this->pdo->prepare('SELECT * FROM client WHERE id = ?');
        $stmt->execute([$id]);
        $row = $stmt->fetch();

        if (!$row) {
            return null;
        }

        $client = new Client($row['firstname'], $row['lastname'], $row['phone']);
        $client->setId((int)$row['id']);
        return $client;
    }


    public function findAll(): array
    {
        $stmt = $this->pdo->query('SELECT * FROM client');
        $clients = [];
        while ($row = $stmt->fetch()) {
            $clients[] = new Client($row['firstname'], $row['lastname'], $row['phone']);
        }

        return $clients;
    }

    public function update(Client $client): bool
{
    // Validate the phone number
    if (!$this->isValidPhone($client->getPhone())) {
        throw new InvalidArgumentException("Invalid phone format.");
    }

    $stmt = $this->pdo->prepare('UPDATE client SET firstname = ?, lastname = ?, phone = ? WHERE id = ?');
    return $stmt->execute([$client->getFirstname(), $client->getLastname(),
        $client->getPhone(), $client->getId()]);
}
    
function isValidPhone($phone) {
    // Remove non-numeric characters
    $phone = preg_replace('/[^0-9]/', '', $phone);

    // Check the length of the number
    if (strlen($phone) != 10) {
        return false;
    }

    // Check the prefix for French numbers (0X)
    if (substr($phone, 0, 1) !== '0') {
        return false;
    }

    // Check the second digit of the prefix
    $secondDigit = substr($phone, 1, 1);
    if ($secondDigit !== '6' && $secondDigit !== '7') {
        return false;
    }

    // The number seems to be in the correct format
    return true;
}

    

    public function delete($id): bool
    {
        $stmt = $this->pdo->prepare('DELETE FROM client WHERE id = ?');
        return $stmt->execute([$id]);
    }
}
